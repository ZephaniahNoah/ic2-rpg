package com.zephaniahnoah.ic2rpg;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.fml.common.registry.GameRegistry;

public class StoneDust extends ModItem {

	public StoneDust(String name, String output) {
		super(name);
		recipe(Block.REGISTRY.getObject(new ResourceLocation(output)));
	}

	public StoneDust(String name, Block output) {
		super(name);
		recipe(output);
	}

	public StoneDust(String name, Item output) {
		super(name);
		recipe(output);
	}

	private void recipe(Object o) {
		ItemStack output;
		if (o instanceof Block)
			output = new ItemStack((Block) o);
		else
			output = new ItemStack((Item) o);
		GameRegistry.addSmelting(this, output, 0.3F);
	}
}
