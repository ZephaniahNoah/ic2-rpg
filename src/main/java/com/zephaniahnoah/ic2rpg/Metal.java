package com.zephaniahnoah.ic2rpg;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.NonNullList;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.oredict.OreDictionary;

public class Metal {
	public Block ore;
	public Item ingot;
	public String name;
	public ModItem crushed;
	public ModItem purified;
	public ModItem dust;
	public ModItem tiny;
	public ItemStack stone;

	public Metal(String block, String item, String metal) {
		ore = Block.REGISTRY.getObject(new ResourceLocation(block));
		ingot = Item.REGISTRY.getObject(new ResourceLocation(item));
		name = metal;
		if (!Main.isClassic) {
			Item stoneDust = Item.REGISTRY.getObject(new ResourceLocation("ic2:dust"));

			if (stoneDust == null)
				Main.isClassic = true;
			else
				stone = new ItemStack(stoneDust, 1, 15);
		}
		Main.METALS.add(this);
	}
}
