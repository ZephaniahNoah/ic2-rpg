package com.zephaniahnoah.ic2rpg;

import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.util.ResourceLocation;

public class MetalCustom extends Metal {
	public MetalCustom(String block, String item, String metal, Item stoneDust) {
		super(block, item, metal);
		stone = new ItemStack(stoneDust);
	}
}
