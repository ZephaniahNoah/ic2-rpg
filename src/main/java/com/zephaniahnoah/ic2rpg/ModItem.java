package com.zephaniahnoah.ic2rpg;

import net.minecraft.item.Item;

public class ModItem extends Item {

    public ModItem(String name) {
        setRegistryName(name);
        setUnlocalizedName(Main.MODID + "." + name);
        Main.ITEMS.add(this);
    }
}
