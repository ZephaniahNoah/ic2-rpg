package com.zephaniahnoah.ic2rpg;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import ic2.api.recipe.Recipes;
import net.minecraft.client.renderer.block.model.ModelResourceLocation;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.init.Blocks;
import net.minecraft.item.Item;
import net.minecraft.item.ItemStack;
import net.minecraft.nbt.NBTTagCompound;
import net.minecraftforge.client.event.ModelRegistryEvent;
import net.minecraftforge.client.model.ModelLoader;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.fml.common.Loader;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.eventhandler.SubscribeEvent;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.relauncher.SideOnly;
import net.minecraftforge.oredict.OreDictionary;

@Mod.EventBusSubscriber(modid = Main.MODID)
@Mod(modid = Main.MODID, name = Main.NAME, version = Main.VERSION, dependencies = "required-after:ic2")
public class Main {
	public static final String MODID = "ic2rpg";
	public static final String NAME = "IC2 RPG";
	public static final String VERSION = "2.1";
	public static List<Item> ITEMS = new ArrayList<Item>();
	public static List<Metal> METALS = new ArrayList<Metal>();
	private static Random rand = new Random();
	public static boolean isClassic = false;

	public static final CreativeTabs tab = (new CreativeTabs(MODID) {
		@Override
		public ItemStack getTabIconItem() {
			return new ItemStack(ITEMS.get(rand.nextInt(ITEMS.size())));
		}
	});

	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		MinecraftForge.EVENT_BUS.register(this);
	}

	@EventHandler
	public void init(FMLInitializationEvent event) {
		for (Metal m : METALS) {
			Recipes.macerator.addRecipe(Recipes.inputFactory.forStack(new ItemStack(m.ore)), null, false, new ItemStack(isClassic ? m.dust : m.crushed, 2));
			if(!isClassic) {
				NBTTagCompound water = new NBTTagCompound();
				water.setInteger("amount", 400);
				Recipes.oreWashing.addRecipe(Recipes.inputFactory.forStack(new ItemStack(m.crushed)), Arrays.asList(new ItemStack[] { new ItemStack(m.purified), new ItemStack(m.tiny, 2), m.stone }), water, false);
				GameRegistry.addSmelting(m.crushed,  new ItemStack(m.ingot), 0.6F);
				GameRegistry.addSmelting(m.purified, new ItemStack(m.ingot), 0.6F);
			}
			GameRegistry.addSmelting(m.dust, new ItemStack(m.ingot), 0.6F);
		}
	}

	@SubscribeEvent
	public static void registerItems(RegistryEvent.Register<Item> event) {
		Item netherrackDust = new StoneDust("dust_netherrack", Blocks.NETHERRACK);
		OreDictionary.registerOre("dustNetherrack", netherrackDust);

		String mod = "divinerpg";

		if (Loader.isModLoaded(mod)) {
			String[] metals = { "realmite", "rupee", "arlemite" };
			for (String m : metals) {
				new Metal(mod + ":" + m + "_ore", mod + ":" + m + "_ingot", m);
			}
		}

		if (Loader.isModLoaded(mod = "aoa3") || Loader.isModLoaded(mod = "nevermine")) {
			Item runicBrick = new ModItem("runic_brick");
			Item barthos    = new StoneDust("dust_barathos_hellstone", mod + ":barathos_hellstone");
			Item baron      = new StoneDust("dust_baron_stone",        mod + ":baron_stone");
			Item runic      = new StoneDust("dust_runic_stone",        runicBrick);
			Item greckon    = new StoneDust("dust_greckon_stone",      mod + ":greckon_stone");
			Item iromine    = new StoneDust("dust_iromine_stone",      mod + ":iromine_stone");
			Item shyrelands = new StoneDust("dust_shyrelands_stone",   mod + ":shyrelands_stone");
			Item mysterium  = new StoneDust("dust_mysterium_stone",    mod + ":mysterium_stone");
			new Metal(mod + ":limonite_ore", mod + ":limonite_ingot", "limonite");
			new Metal(mod + ":rosite_ore",   mod + ":rosite_ingot",   "rosite");
			new MetalCustom(mod + ":emberstone_ore", mod + ":emberstone_ingot", "emberstone", netherrackDust);
			new MetalCustom(mod + ":blazium_ore",    mod + ":blazium_ingot",    "blazium",    barthos);
			new MetalCustom(mod + ":baronyte_ore",   mod + ":baronyte_ingot",   "baronyte",   baron);
			new MetalCustom(mod + ":varsium_ore",    mod + ":varsium_ingot",    "varsium",    baron);
			new MetalCustom(mod + ":elecanium_ore",  mod + ":elecanium_ingot",  "elecanium",  runic);
			new MetalCustom(mod + ":ghoulish_ore",   mod + ":ghoulish_ingot",   "ghoulish",   greckon);
			new MetalCustom(mod + ":ghastly_ore",    mod + ":ghastly_ingot",    "ghastly",    greckon);
			new MetalCustom(mod + ":lyon_ore",       mod + ":lyon_ingot",       "lyon",       iromine);
			new MetalCustom(mod + ":shyrestone_ore", mod + ":shyrestone_ingot", "shyrestone", shyrelands);
			new MetalCustom(mod + ":mystite_ore",    mod + ":mystite_ingot",    "mystite",    mysterium);
		}

		for (Metal m : METALS) {
			if (!isClassic) {
				m.crushed  = new ModItem("crushed_" + m.name);
				m.purified = new ModItem("purified_" + m.name);
				m.tiny     = new ModItem("dust_" + m.name + "_small");
				OreDictionary.registerOre("crushed" + capitalize(m.name), m.crushed);
				OreDictionary.registerOre("crushedPurified" + capitalize(m.name), m.purified);
				OreDictionary.registerOre("dustTiny" + capitalize(m.name), m.tiny);
			}
			m.dust = new ModItem("dust_" + m.name);
			OreDictionary.registerOre("dust" + capitalize(m.name), m.dust);
		}

		for (Item item : ITEMS) {
			item.setCreativeTab(Main.tab);
			event.getRegistry().register(item);
		}
	}

	@SideOnly(Side.CLIENT)
	@SubscribeEvent
	public static void registerModels(ModelRegistryEvent e) {
		for (Item item : ITEMS) {
			registerRender(item);
		}
		// ITEMS.clear(); Breaks creative tab
	}

	private static String capitalize(String word) {
		return word.substring(0, 1).toUpperCase() + word.substring(1);
	}

	private static void registerRender(Item item) {
		ModelLoader.setCustomModelResourceLocation(item, 0,
				new ModelResourceLocation(item.getRegistryName(), "inventory"));
	}
}
