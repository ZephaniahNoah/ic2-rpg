with open('en_us.lang', 'r') as file:
    datasplit = file.read().split('\n')
    
    for line in datasplit:
        split = line.split("=")
        if(len(split)>1):
            first = split[0]
            second = split[1]
            #print("first "+first+" - second "+second)

            newString = ""
            for letter in first:
                
                if letter.isupper():
                    newString+="_"+letter.lower()
                else:
                    newString+=letter
            done = newString + "=" + second
            print(done)
