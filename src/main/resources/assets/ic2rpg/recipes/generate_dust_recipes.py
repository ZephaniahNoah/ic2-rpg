metals = ["realmite","rupee","arlemite","emberstone","blazium","baronyte","varsium","elecanium","ghoulish","ghastly","lyon","shyrestone","mystite","limonite","rosite"]

for m in metals:
    f = open("craft_" + m + "_dust.json", "w+")

    f.write("{\n  \"type\": \"minecraft:crafting_shaped\",\n  \"pattern\": [\n    \"XXX\",\n    \"XXX\",\n    \"XXX\"\n  ],\n  \"key\": {\n    \"X\": {\n      \"item\": \"ic2rpg:dust_" + m + "_small\"\n    }\n  },\n  \"result\": {\n      \"item\": \"ic2rpg:dust_" + m + "\"\n  }\n}")

    f.close()
