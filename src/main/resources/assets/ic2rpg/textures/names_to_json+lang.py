import os

folder = "items"

files = os.listdir(folder)

for f in files:
    name = f[:-4]

    lang = "item.ic2rpg." + name + ".name="

    if name.startswith("crushed_"):
        n = name.replace("crushed_", "")
        lang += "Crushed " + n.capitalize() + " Ore"
    elif name.startswith("purified_"):
        n = name.replace("purified_", "")
        lang += "Purified Crushed " + n.capitalize() + " Ore"
    elif name.startswith("dust_"):
        n = name.replace("dust_", "")
        if name.endswith("_small"):
            n = n.replace("_small", "")
            lang += "Tiny Pile of " + n.capitalize() + " Dust"
        else:
            lang += n.capitalize() + " Dust"
    
    print(lang)
    
    f = open("models/" + name + ".json", "w+")

    f.write("{\n  \"parent\": \"item/generated\",\n  \"textures\": {\n    \"layer0\": \"ic2rpg:items/" + name + "\"\n  }\n}")

    f.close()
