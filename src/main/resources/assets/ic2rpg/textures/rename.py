import os

folder = "items"

files = os.listdir(folder)

for f in files:
    print(f)
    
    name = ""

    for letter in f:
        if letter.isupper():
            name+="_"+letter.lower()
        else:
            name+=letter
    name = name[5:]
    
    print("is now " + name)
    
    os.rename(folder + "/" + f, folder + "/" + name)
